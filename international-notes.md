# Allgemeines

Bisher hat dieses Projekt nur politische Akteur:innen erfasst, die in der Bundesrepublik Deutschland oder auf EU-Ebene aktiv sind.

Aktuell ist offen, wie sinnvoll eine Ausweitung auf andere Länder ist. Diese Datei dient als Zwischenspeicher für eventuelle Kandidaten-Links.

# Österreich

## Die Partei

- https://wien.rocks/@diePARTEI (Die PARTEI Wien)
- https://graz.social/@diePARTEI_stmk (Die PARTEI Steiermark)